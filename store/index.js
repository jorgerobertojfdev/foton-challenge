import Reactotron from 'reactotron-react-native';
import sagaPlugin from 'reactotron-redux-saga';
import { reactotronRedux } from 'reactotron-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import {combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';

import bookReducer from './books/index';
import rootSaga from './sagas'

const rootReducer = combineReducers({ 
  books: bookReducer 
});

let sagaMiddleware = createSagaMiddleware(); 

let store = createStore(
  rootReducer, 
  compose(
    applyMiddleware(sagaMiddleware)
  )
);

if (__DEV__) {
  const reactotron = Reactotron
    .configure({
      name: 'Foton-Challenge',
      host: '192.168.0.101'
    })
    .useReactNative()
    .use(reactotronRedux())
    .use(sagaPlugin())
    .connect();

  const sagaMonitor = reactotron.createSagaMonitor();
  
  sagaMiddleware = createSagaMiddleware({ sagaMonitor });

  store = reactotron.createStore(
    rootReducer,
    compose(
      applyMiddleware(sagaMiddleware),
    )
  );

  global.tron = reactotron;
}

sagaMiddleware.run(rootSaga);

export default store;
