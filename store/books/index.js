import {
  REQUEST_BOOKS,
  REQUEST_BOOKS_SUCCESS,
  REQUEST_BOOKS_FAILED,  
  REQUEST_BOOKS_PAGINATION,
  REQUEST_BOOKS_PAGINATION_SUCCESS,
  REQUEST_BOOKS_PAGINATION_FAILED,
  SHOW_BOOK,
  SHOW_BOOK_SUCCESS,
  SHOW_BOOK_FAILED
} from './constants';

const initalState = {
  data: [],
  selectedBook: null,
  loading: false, 
  error: false
};

export default function bookStore(state = initalState, action) {
  switch (action.type) { 
    case REQUEST_BOOKS_PAGINATION:
    case REQUEST_BOOKS:
    case SHOW_BOOK:
      return {...state, loading: true}
    case REQUEST_BOOKS_PAGINATION_SUCCESS:
      return {...state, data: state.data.concat(action.payload.books), loading: false}        
    case REQUEST_BOOKS_SUCCESS:
      return {...state, data: action.payload.books, loading: false}        
    case SHOW_BOOK_SUCCESS:
      return {...state, selectedBook: action.payload.book, loading: false}    
    case REQUEST_BOOKS_PAGINATION_FAILED:
    case REQUEST_BOOKS_FAILED:
    case SHOW_BOOK_FAILED:
      return {...state, data: [], loading: false, error: true}
    break; 
    default:
      return state;
  }
};
