import {
  REQUEST_BOOKS,
  REQUEST_BOOKS_PAGINATION,
  SHOW_BOOK
} from './constants';

export const searchBooks = (payload) => ({
  type: REQUEST_BOOKS,
  payload
})

export const paginateBooks = (payload) => ({
  type: REQUEST_BOOKS_PAGINATION,
  payload
})

export const showBook = (payload) => ({
  type: SHOW_BOOK,
  payload
})
