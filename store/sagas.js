import { takeLatest, call, put, all } from 'redux-saga/effects';
import Reactotron from 'reactotron-react-native';

function* listBooks({ payload: { search }}) {
  try {
    const url = `https://www.googleapis.com/books/v1/volumes?q=${search}`;
    const response = yield call(fetch, url);
    const books = (yield response.json()).items || [];

    Reactotron.log(books);

    yield put({ type: 'REQUEST_BOOKS_SUCCESS', payload: {books} });
  } catch (e) {
    yield put({ type: 'REQUEST_BOOKS_FAILED', message: e.message });
  }
}

function* showBook({ payload: { id }}) {
  try {
    const url = `https://www.googleapis.com/books/v1/volumes/${id}`;
    const response = yield call(fetch, url);
    const book = yield response.json();
    
    Reactotron.log(book);

    yield put({ type: 'SHOW_BOOK_SUCCESS', payload: {book} });
  } catch (e) {
    yield put({ type: 'SHOW_BOOK_FAILED', message: e.message });
  }
}

function* paginateBooks({ payload: { search, page } }) {
  try {
    const url = `https://www.googleapis.com/books/v1/volumes?q=${search}&startIndex=${page}`;
    const response = yield call(fetch, url);
    const books = (yield response.json()).items || [];

    Reactotron.log('books', books);

    yield put({ type: 'REQUEST_BOOKS_PAGINATION_SUCCESS', payload: {books} });
  } catch (e) {
    yield put({ type: 'REQUEST_BOOKS_PAGINATION_FAILED', message: e.message });
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest('REQUEST_BOOKS', listBooks),
    takeLatest('SHOW_BOOK', showBook),
    takeLatest('REQUEST_BOOKS_PAGINATION', paginateBooks),
  ])
}
