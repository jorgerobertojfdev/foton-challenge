import React, {Fragment} from 'react';
import {Route} from "react-router-native";
import {Text, View} from 'react-native';
import {Content, Menu, MenuItem} from './components/Scaffold';
import Icon from 'react-native-vector-icons/Feather';

export default ({component: Component, ...rest}) => (
  <Route {...rest} render={matchProps => (
      <Fragment>
        <Content>
          <Component {...matchProps} />
        </Content>
        {matchProps.match.path !== '/book/:id' && (<Menu>
          <MenuItem to="/">
            <Fragment>
              <Icon name="home" size={20} color="#000000" />   
              <Text>Home</Text>
            </Fragment>
          </MenuItem>
          <MenuItem color="green" to="/book-list">
            <Fragment>
              <Icon name="search" size={20} color="#000000" />
              <Text>Buscar Livros</Text>
            </Fragment>
          </MenuItem>
        </Menu>)}          
      </Fragment>
  )}

  />
);
