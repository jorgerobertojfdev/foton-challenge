/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { NativeRouter } from "react-router-native";
import store from './store';

import Home from './screens/Home';
import BookList from './screens/BookList';
import BookDetail from './screens/BookDetail';
import LayoutRouter from './LayoutRouter.js';
import {BackButton} from "react-router-native";
import {Container, Content} from './components/Scaffold';

type Props = {};
class App extends Component<Props> {

  render() {
    return (
      <Provider store={store}>
        <NativeRouter>
          <Container>
            <Content>
              <BackButton />
              <LayoutRouter exact path="/" component={Home} />
              <LayoutRouter path="/book-list" component={BookList} />
              <LayoutRouter path="/book/:id" component={BookDetail} />
            </Content>
          </Container>
        </NativeRouter>
      </Provider>
    );
  }
};

export default App;
