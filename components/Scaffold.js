import styled from 'styled-components/native';
import { Link } from "react-router-native";

export const Container = styled.View`
  display: flex;
  background-color: #fde200;
  height: 100%;
  width: 100%;
`;

export const Content = styled.View`
  flex: 9;
  width: 100%;
`;

export const Menu = styled.View`
  border-top-color: #000;
  border-top-width: 1;
  flex: 1;
  flex-direction: row;
  width: 100%;
`;

export const MenuItem = styled(Link)`
  flex: 1;
  align-items: center;
  justify-content: center;
  width: 50%;
`;

export const Button = styled(Link)`
  align-items: center;
  justify-content: center;
  width: 50%;
  height: 50px;
  border-width: 1px;
  border-color: #000;
  border-radius: 5px;
`;

export const SearchContainer = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 40;
  width: 100%;
  padding: 0 20px;
`

export const IconFilterContainer = styled.View`
  margin-left: 15px;
`;

export const SearchInput = styled.TextInput`
  border-radius: 50;
  border-color: #000;
  border-width: 1;
  padding: 0 20px;
  height: 40;
  width: 88%;
  background-color: #FFF;
`;

export const HeaderDetailBook = styled.View`
  flex: 3;
  padding: 15px;
`;

export const DescriptionDetailBook = styled.View`
  flex: 1;
  flex-direction: column;
  padding: 15px;
  background-color: #FFF;
`;
