import React, {Component} from 'react';
import { AppRegistry, Image, Text } from 'react-native';
import styled from 'styled-components/native';
import { Link } from 'react-router-native';

const BookItemContainer = styled(Link)`
  flex: 1;
  flex-basis: 0;
  margin: 4px;
`;

const BookItem = ({ images, title, id }) => {
  const uri = images && images.thumbnail || 'https://via.placeholder.com/128x143';

  return (
    <BookItemContainer to={`/book/${id}`}>
      <Image
        style={{width: 113, height: 123}}
        source={{uri}}
      />
    </BookItemContainer>
  );
};

export default BookItem;
