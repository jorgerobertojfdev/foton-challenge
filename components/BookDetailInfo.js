import React, { Component, Fragment } from 'react';
import styled from 'styled-components/native';
import StarRating from 'react-native-star-rating';
import {TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { withRouter } from "react-router-native";

const Container = styled.View`
  flex: 1;
  flex-direction: row;
`;

const InfoContainer = styled.View`
  flex: 1;
  margin-left: 15;
`;

const BackButtonContainer = styled.View`
  width: 100%;
  height: 30;
`;

const Cover = styled.View`
  width: 93;
  height: 118;
  elevation: 5;
  background-color: #fde200;
`;
const BookImage = styled.Image`
  width: 93;
  height: 118;
`;

const Title = styled.Text`
  font-weight: bold;
  line-height: 20;
  font-size: 18;
`;

const Authors = styled.Text`
  font-size: 14;
  color: #666;
  margin-bottom: 10;
  margin-top: 5;
`;

const Price = styled.Text`
  fontWeight: bold;
  fontSize: 18;
  margin-right: 5;
`;

const saleability = (value, bookStatus) => {
  const status = {
    NOT_FOR_SALE: 'Não está á venda.'
  };

  tron.log(value);

  return value ? `R$ ${value}`: status[bookStatus];
};

class BookDetailInfo extends Component {
  state = {
    rating: 2.5
  }

  updateRating = (rating) => {
    this.setState(prev => ({
      ...prev,
      rating
    }));
  }

  render() {
    const {book} = this.props;
    const {rating} = this.state;
    const hitSlop = {left: 15, right: 15, top: 15, bottom: 15};

    tron.log(book);

    return (
      <Fragment>
        <BackButtonContainer>
          <TouchableWithoutFeedback hitSlop={hitSlop} onPress={() => this.props.history.goBack()}>
            <Icon name="arrow-left" size={20} />
          </TouchableWithoutFeedback>
        </BackButtonContainer>
        <Container>
          <Cover>
            <BookImage source={{uri: book.volumeInfo.imageLinks.thumbnail}} />
          </Cover>
          <InfoContainer>
            <Title>
              {book.volumeInfo.title}
            </Title>
            <Authors>
              By {book.volumeInfo.authors && book.volumeInfo.authors.join(', ')}
            </Authors>
            <Container>
              <Price>
                {
                  saleability(
                    book.saleInfo.listPrice && book.saleInfo.listPrice.amount, 
                    book.saleInfo.saleability
                  )
                }
              </Price>
              <StarRating 
                starStyle={{marginTop: 6}} 
                starSize={15} 
                disabled={false} 
                maxStars={5} 
                rating={rating} 
                selectedStar={this.updateRating}
              />
            </Container>
          </InfoContainer>
        </Container>
      </Fragment>
    );
  }
}
export default withRouter(BookDetailInfo);
