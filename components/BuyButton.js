import React, { Component } from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity} from 'react-native';

const BuyButtonContainer = styled.View`
  background-color: #4f92e5;
  elevation: 5;
  font-size: 18px;
  padding: 10px 50px;
  border-radius: 50;
  margin: 0 10px;
  justify-content: center;
  align-items: center;
`;

const BuyButtonText = styled.Text`
  color: #FFF;
`;

export default class BuyButton extends Component {
  render() {
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={() => console.log('click')}>
        <BuyButtonContainer>
            <BuyButtonText>BUY</BuyButtonText>
        </BuyButtonContainer>   
      </TouchableOpacity>
    );
  }
};
