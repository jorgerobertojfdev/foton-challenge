import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, Text } from 'react-native';
import styled, {css} from 'styled-components/native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { changeSearchType } from '../store/books/actions';

const Container = styled.View`
  margin-top: 30px;
  width: 150px;
  flex: 1;
  flex-direction: row;
  height: 50px;
`;
const Option = styled.View`
  width: 50%;
  height: 30px;
  background-color: ${prop => prop.active ? '#333' : '#CCC'};
  padding: 5px;

  ${item => 
    item.primary ? css`
    border-top-left-radius: 5px; 
    border-bottom-left-radius: 5px; 
    border-right-width: 1px;
    border-right-color: #000;
  ` :
  `
    border-top-right-radius: 5px; 
    border-bottom-right-radius: 5px;
  `}
`;

const TextOption = styled.Text`
  color: #FFF;
  text-align: center;
`;

class Toggle extends Component {
  toggleSearch = (searchType) => {
    this.props.changeSearchType({searchType});
  }
  render() {
    const {type} = this.props;

    return (
      <Container>
        <Option primary active={type === 'online'}>
          <TouchableWithoutFeedback onPress={() => this.toggleSearch('online')}>
            <TextOption>Online</TextOption>
          </TouchableWithoutFeedback>
        </Option>
        <Option active={type === 'local'}>
          <TouchableWithoutFeedback onPress={() => this.toggleSearch('local')}>
            <TextOption>Local</TextOption>
          </TouchableWithoutFeedback>
        </Option>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  type: state.books.searchType
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ changeSearchType }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Toggle);
