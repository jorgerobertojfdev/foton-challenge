import React, { Component } from 'react';
import styled from 'styled-components/native';

const Container = styled.View`
flex: 1;
flex-direction: row;
`;

const ActionsContainer = styled(Container)`
flex: 1;
justify-content: flex-end;
flex-direction: row;
marginTop: 10;
width: 72%;
`;

const PagesContainer = styled.View`
width: 28%;
margin-top: 6px;
`;

const PagesText = styled.Text`
marginTop: 15;
textAlign: center;
`;

import BuyButton from './BuyButton.js';
import HeartButton from './HeartButton.js';

export default class Actions extends Component {
  render() {
    const {book} = this.props;

    return (
      <Container>
        <PagesContainer>
          <PagesText>
            {book.volumeInfo.pageCount} pages
          </PagesText>
        </PagesContainer>          
        <ActionsContainer>     
          <BuyButton />      
          <HeartButton />      
        </ActionsContainer>
      </Container>
      );
  }
}
