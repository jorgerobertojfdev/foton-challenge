import React, { Component } from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Feather';
import {TouchableWithoutFeedback} from 'react-native';

const HeartButtonContainer = styled.View`
  elevation: 5;
  font-size: 18px;
  border-radius: 50;
  margin: 0 10px;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.liked ? '#a0303f' : '#db4b5e'};
  padding: 13px;
`;

export default class HeartButton extends Component {
  state = {
    liked: false
  }

  toggleLike = () => {
    this.setState(prev => ({
      liked: !prev.liked
    }));
  }

  render() {
    const {liked} = this.state;

    return (
      <TouchableWithoutFeedback onPress={this.toggleLike}>
        <HeartButtonContainer liked={liked}>
          <Icon name="heart" color="#FFF" />
        </HeartButtonContainer>
      </TouchableWithoutFeedback>
    );
  }
}
