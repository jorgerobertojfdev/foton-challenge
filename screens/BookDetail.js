import React, { Component } from 'react';
import {ScrollView, Text, SafeAreaView} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showBook } from '../store/books/actions';
import BookDetailInfo from '../components/BookDetailInfo.js';
import { 
  DescriptionDetailBook,
  HeaderDetailBook
} from '../components/Scaffold';
import Actions from '../components/Actions.js';

class BookDetail extends Component {
  componentDidMount() {
    const {showBook, match: { params: {id}}} = this.props;

    showBook({id});
  }

  render() {
    const {book} = this.props;

    return book && (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <HeaderDetailBook>
            <BookDetailInfo book={book} />
            <Actions book={book} />
          </HeaderDetailBook>
          {book.volumeInfo.description && (<DescriptionDetailBook>
            <Text style={{lineHeight: 30}}>{book.volumeInfo.description}</Text>
          </DescriptionDetailBook>)}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  book: state.books.selectedBook
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ showBook }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
