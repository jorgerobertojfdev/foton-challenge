import React, { Component } from 'react';
import {Platform, SafeAreaView, Text, View, TouchableHighlight} from 'react-native';
import { 
  Button
} from '../components/Scaffold';
const MainView = Platform.OS === 'ios' ? SafeAreaView : View;

class Home extends Component {

  render() {
    return (
      <MainView style={{flex: 1, flexDirection:'row', justifyContent: 'center',  alignItems: 'center'}}>
        <Button to="/book-list" component={TouchableHighlight} underlayColor="#FFF">
          <Text>Ir para busca de livros :)</Text>          
        </Button>
      </MainView>
    );
  }
}

export default Home;
