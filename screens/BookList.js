import React, { Component } from 'react';
import {FlatList, View, Text, SafeAreaView, Platform, TouchableWithoutFeedback} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';

import { paginateBooks, searchBooks } from '../store/books/actions';
import BookItem from '../components/BookItem';
import { debounce } from '../utils/methods';
import { 
  SearchContainer,
  SearchInput
} from '../components/Scaffold';

const uniqueId = () => {
  return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
};

const MainView = Platform.OS === 'ios' ? SafeAreaView : View;

class BookList extends Component {
  state = {
    page: 0,
    searching: false,
    search: null
  }

  refreshBookSearch = () => {
    const {search, searchBooks} = this.props;

    searchBooks({ search });
  }

  searchBook = debounce((search) => {
    const {searchBooks} = this.props;
    searchBooks({search});
  }, 300)
  
  loadMoreItems = debounce(() => {
    const {search} = this.props;
    const {page: oldPage} = this.state;
    const page = oldPage + 1;

    this.setState(prev => ({ ...prev, page }));

    this.props.paginateBooks({ search, page });
  }, 300)

  setSearching = () => {
    this.setState(prev => ({ ...prev, searching: true }));
  }

  unsetSearching = () => {
    this.setState(prev => ({ ...prev, searching: false }));
  }

  render() {
    const {searching} = this.state;
    const {books, loading} = this.props;
    const hitSlop = {left: 15, right: 15, top: 15, bottom: 15};

    return (
      <MainView style={{flex: 1}}>
        <SearchContainer>
          {searching && (
            <>
              <TouchableWithoutFeedback hitSlop={hitSlop} onPress={this.unsetSearching}>
                <Icon name="arrow-left" size={27} />
              </TouchableWithoutFeedback>
              <SearchInput onChangeText={this.searchBook} />
            </>
          )}
          {!searching && (
            <>
            <Icon name="menu" size={27} />
            <View style={{borderBottomColor: '#d2bc07', borderBottomWidth: 2, paddingTop: 15, paddingBottom: 15}}>
              <Text style={{fontSize: 20}}>Design Books</Text>
            </View>
            <TouchableWithoutFeedback hitSlop={hitSlop} onPress={this.setSearching}>
              <Icon name="search" size={27} />
            </TouchableWithoutFeedback>
            </>
          )}
        </SearchContainer>
        <View style={{flex: 7}}>
          <FlatList
            data={books}
            numColumns={3}
            refreshing={loading}
            onRefresh={this.refreshBookSearch}
            onEndReached={this.loadMoreItems}
            keyExtractor={item => uniqueId()}
            renderItem={({item}) => (
             <BookItem
              id={item.id}
              title={item.volumeInfo.title}
              images={item.volumeInfo.imageLinks} />
            )}
          />
        </View>
      </MainView>
    );
  }
}

const mapStateToProps = state => ({
  books: state.books.data,
  loading: state.books.loading,
  type: state.books.searchType
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ paginateBooks, searchBooks }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
